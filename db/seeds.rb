# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create!( name: "Example User",
              email: "example@railstutorial.org",
              address: "Ha Dong, Ha Noi",
              mobile: "08756753245",
              password: "123456",
              password_confirmation: "123456",
              admin: true)

40.times do |n|
  name = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  address = "hanoi"
  address = Faker::Address.street_address
  mobile = Faker::PhoneNumber.cell_phone
  password = "123456"

  User.create!( name: name,
                email: email,
                address: address,
                mobile: mobile,
                password: password,
                password_confirmation: password)
end
arr = ["Shirt", "T-Shirt", "Polo-Shirt", "Bag", "Jacket", "Shoes"]
50.times do |y|
  title = Faker::Commerce.product_name
  type_product = arr[rand(6)]
  price = Faker::Commerce.price
  quantity = Faker::Number.number(digits: 2)
  content = Faker::Lorem.paragraph_by_chars
  number = Faker::Number.between(from: 1, to: 12)
  Product.create!(
    title: title,
    type_product: type_product,
    price: price,
    quantity: quantity,
    content: content,
    url_image: "shop/product#{number}.jpg"
  )
end
