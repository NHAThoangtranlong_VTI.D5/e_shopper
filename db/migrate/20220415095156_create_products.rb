class CreateProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :products do |t|
      t.string :title
      t.string :type_product
      t.float :price
      t.integer :quantity
      t.text :content
      t.text :url_image

      t.timestamps
    end
  end
end
