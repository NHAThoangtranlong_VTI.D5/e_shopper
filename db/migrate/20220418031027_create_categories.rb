class CreateCategories < ActiveRecord::Migration[6.1]
  def change
    create_table :categories do |t|
      t.string :title
      t.text :content
      t.text :url_image

      t.timestamps
    end
  end
end
