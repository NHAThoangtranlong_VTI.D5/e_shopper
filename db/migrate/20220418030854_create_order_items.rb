class CreateOrderItems < ActiveRecord::Migration[6.1]
  def change
    create_table :order_items do |t|
      t.integer :product_id
      t.integer :order_id
      t.integer :user_id
      t.boolean :status
      t.float :price
      t.integer :quantity
      t.text :content

      t.timestamps
    end
  end
end
