class UsersController < ApplicationController
  before_action :find_user, only: [:show, :edit, :destroy, :update]

  def new
  end
  
  def show
    @products = Product.paginate(page: params[:page])
  end

  def edit
    @user = current_user
  end

  def index
    @users = User.paginate(page: params[:page])
  end
  def create
    @user = User.new(user_params) 
    if @user.save
      redirect_to @user
    else
      render 'new'
    end
  end

  def update
    if @user.update(user_params)
      flash[:success] = "Profile was successfully updated."
      redirect_to @user
    else
      flash[:danger] = "Update profile fail."
      render 'edit'
    end
  end

  def destroy
    if @user.destroy
      flash[:success] = "User_deleted"
    else
      flash[:danger] = "Delete_fail"
    end
    redirect_to users_url
  end

  private
      
    def user_params
      params.require(:user).permit(:name, :email, :address, :mobile, :password, :password_confirmation)
    end
  
    def find_user
      @user = User.find_by id: params[:id]
    end
end
