class OrdersController < ApplicationController
  before_action :find_params, only: [:show]
  before_action :authenticate_user!
  def new
    @order = Order.new
    @order.order_items.build
  end
  def show
    @order_items = current_order.order_items
  end
  def create
    product = Product.find_by(id: params[:id])
    @order = current_order
    @order_items = @order.order_items.build do |o|
      o.product_id = product.id
      o.user_id = current_user.id
      o.order_id = current_order.id
      o.status = 0
      o.price = product.price
      o.quantity = 1
    end
    if @order_items.save
      flash[:success] = "Add product to cart successfully."
      redirect_to products_url
    else 
      flash[:danger] = "Add product to cart fail"
    end 
    session[:order_id] = @order.id
  end
  def add_quantity

    @order = current_order
    @order_items = @order.order_items.find_by(id: params[:id])
    @order_items.quantity += 1
    if @order_items.save
      flash[:success] = "Add quantity success."
      @order_items = @order.order_items
    render :show
    else
      flash[:danger] = "Add quantity fail"
    end
  end
  def reduce_quantity
    @order = current_order
    @order_items = @order.order_items.find_by(id: params[:id])
    if @order_items.quantity > 1
      @order_items.quantity -= 1
      @order_items.save
      flash[:success] = "Reduce quantity success."
    else 
      @order_items.destroy
      flash[:success] = "Delete product cart"
    end
    @order_items = @order.order_items
    render :show
  end

  def destroy
    @order = current_order
    order_items = @order.order_items.find_by(id: params[:id])
    if order_items.destroy
      flash[:success] = "Delete product cart success."
    else 
      flash[:danger] = "Delete product cart fail."
    end
    @order_items = @order.order_items
    render :show
  end
  private
    def order_params
      params.require(:order).permit :user_id,:subTotal,:tax,:total,
       order_items_attributes: [:product_id, :order_id, :status, :price, :quantity, :content]
    end
    def find_params
      @order = Order.find_by(id: params[:id])
    end
end
