class ProductsController < ApplicationController
  before_action :find_params, except: %i[create new index]
  before_action :authenticate_user!, except: [:index, :show]

  def index
    @products = Product.paginate(page: params[:page], per_page: 15)
    # @product = Product.all
  end

  def show
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      flash[:success] = "Add product success."
      redirect_to @product
    else
      flash[:danger] = "Add product fail"
      redirect_to @product
    end
  end

  def update
    if @product.update(product_params)
      flash[:success] = "Product was successfully updated."
      redirect_to products_url
    else
      flash[:danger] = "Product update fail"
      render :edit
    end

  end

  def destroy
    if @product.destroy
      flash[:success] = "Product_deleted"
    else
      flash[:danger] = "Delete_product_fail"
    end
    redirect_to products_url
  end

  private
    def product_params
      params.require(:product).permit :title, :type_product, :price, :quantity, :content, :url_image
    end

    def find_params
      @product = Product.find_by(id: params[:id])
    end
end
