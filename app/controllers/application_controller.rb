class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_locale
  helper_method :current_order

  def current_order
    if !user_session[:order_id].nil? || Order.find_by(user_id: current_user.id)
      Order.find_by(user_id: current_user.id )
    else
      Order.new do |o|
        o.user_id = current_user.id
      end
    end
  end
  private

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def default_url_options
    {locale: I18n.locale}
  end
  

  protected
 
  def configure_permitted_parameters
    added_attrs = [:name, :address, :mobile, :email, :password, :password_confirmation, :remember_me]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end
end
