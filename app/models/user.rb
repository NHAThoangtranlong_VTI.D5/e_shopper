class User < ApplicationRecord
  has_many :order_item, dependent: :destroy
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
end
