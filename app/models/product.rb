class Product < ApplicationRecord
  has_many :order_items, dependent: :destroy

  validates :title, presence: true, length: {maximum: Settings.products.max_title_length}
  validates :type_product, presence: true, length: {maximum: Settings.products.max_type_length}
  validates :price, presence: true, 
            numericality: {
              less_than_or_equal_to: Settings.products.max_price,
              greater_than_or_equal_to: Settings.products.min_price}
  validates :quantity, presence: true, 
            numericality: {
            less_than_or_equal_to: Settings.products.max_quantity,
            greater_than_or_equal_to: Settings.products.min_quantity,
            only_integer: true}
  validates :content, presence: true, length: {maximum: Settings.products.max_content_length}
end
