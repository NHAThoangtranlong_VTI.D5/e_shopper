module OrdersHelper
  def total
    sum = 0
    @order_items.each do |o| 
      sum += o.price * o.quantity
    end
    sum.round(2)
  end
end
