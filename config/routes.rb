Rails.application.routes.draw do
  get 'products/index'
  root to: 'static_pages#home'
  get '/home', to: 'static_pages#home'
  get 'add_cart', to: 'orders#create'
  get 'cart', to: 'orders#show'
  delete 'delete_cart', to: 'orders#destroy'
  devise_for :users
  get 'add_quantity', to: 'orders#add_quantity'
  get 'reduce_quantity', to: 'orders#reduce_quantity'
  scope "(:locale)", locale: /en|vi/ do
    resources :users do
      # get "login" => "devise/sessions#new"
      # post "signin" => "devise/sessions#create"
      # delete "signout" => "devise/sessions#destroy"
      # get "register" => "devise/registrations#new"
    end
    resources :products
  end
  resource :order, only: [:create, :update, :destroy, :add_quantity]
  resource :user, only: [:edit, :update]
end
