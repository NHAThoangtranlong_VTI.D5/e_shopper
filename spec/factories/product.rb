FactoryBot.define do
  factory :product do
		title {Faker::Commerce.product_name}
		type_product {"T_Shirt"}
		price {Faker::Commerce.price}
		quantity {Faker::Number.number(digits: 2)}
		content {Faker::Lorem.paragraph_by_chars}
		number = Faker::Number.between(from: 1, to: 12)
		url_image {"shop/product#{number}.jpg"}
	end
end
