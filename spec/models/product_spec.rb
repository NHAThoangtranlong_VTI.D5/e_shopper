require "rails_helper"

RSpec.describe Product, type: :model do
  let!(:product) do
    FactoryBot.create :product, title: "Black_Shirt", type_product: "T_Shirt",
    price: 34.45, quantity: 23, content: "A good shirt", url_image: "shop/product4.jpg"
  end
  describe "Associations" do
    it "has many order_items" do
      is_expected.to have_many(:order_items)
    end
  end

  describe "Validations" do
    before {product.save}
    [:title, :type_product, :price, :quantity].each do |field|
      it do 
        is_expected.to validate_presence_of(field)
      end
    end

    it do
      is_expected.to validate_length_of(:title).is_at_most(Settings.products.max_title_length)
    end
      
    it do
      is_expected.to validate_length_of(:type_product).is_at_most(Settings.products.max_type_length)
    end

    it do
      is_expected.to validate_numericality_of(:price)
    end

    it do
      is_expected.to validate_numericality_of(:quantity)
    end

    it do
      is_expected.to validate_length_of(:content).is_at_most(Settings.products.max_content_length)
    end
  end

end